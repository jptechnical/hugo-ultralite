---
title: Home
---

[<img src="https://simpleicons.org/icons/gitlab.svg" style="max-width:10%;min-width:30px;float:right;" alt="Github repo" />](https://gitlab.com/jptechnical/hugo-ultralite)

# Ultralite for HUGO

## All I wanted was a basic HUGO template...

Was that so hard to ask? I see a TON of so-called minimalist templates that take
all kind of extra work to implement. I really, REALY, wanted this to be a simple
blog and a place I can focus on writing, instead I am piecing together stuff.
So... in the spirit of K.I.S.S., I am writing this much simplified template. 

## Based on a series of blog posts and a existing minimal template...

I draw inspiration from the  [XMin Hugo theme written by Yihui
Xie](https://yihui.org), if he can do it in under 130 lines of code and have a
functional template that looks good, I should at least be able to do that in 200
lines ;-). 

```bash
find . \( -name '*.html' -o -name '*.css' \) | xargs wc -l
```

```bash
      10 ./layouts/404.html
      12 ./layouts/_default/single.html
      20 ./layouts/_default/list.html
      13 ./layouts/_default/terms.html
       0 ./layouts/partials/foot_custom.html
       0 ./layouts/partials/head_custom.html
      10 ./layouts/partials/footer.html
      25 ./layouts/partials/header.html
      51 ./static/css/style.css
       7 ./static/css/fonts.css
     148 total
```

Not to shabby. I certainly didn't do as efficient a job, but I prefer a little
breathing room on my code blocks.

```yaml
baseURL: "http://ultralite.dev/"
languageCode: "en-us"
title: "[ultralite]# dev|it|blog"
theme: "hugo-ultralite"
# googleAnalytics: "abc123"
# disqusShortname: ""
footnotereturnlinkcontents: "↩"
pygmentscodefences: true
pygmentsStyle: dracula
pygmentsCodefencesGuessSyntax: true
# markup:
#   highlight:
#     style: dracula

permalinks:
  note: "/note/:year/:month/:day/:slug/"
  post: "/post/:year/:month/:day/:slug/"

menu:
  main:
    - name: Home
      url: ""
      weight: 1
    # - name: About
    #   url: "about/"
    #   weight: 2
    # - name: Categories
    #   url: "categories/"
    #   weight: 3
    # - name: Tags
    #   url: "tags/"
    #   weight: 4
    # - name: Subscribe
    #   url: "index.xml"

params:
  description: "A super light-weight website."
  footer: "&copy; [JP Technical](https://jptechnical.com) 2021 | [Gitlab](https://gitlab.com/jptechnical/ultralite) | [Twitter](https://twitter.com/jptechnical)"

markup:
  goldmark:
    renderer:
      unsafe: true
```
